package com.att.training.demo.user;

import com.att.training.demo.api.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class UserRepositoryTest {

    private static final User USER_101 = new User(101L, "Michael", "Jordan", "https://randomuser.me/api/portraits/men/1.jpg");
    private UserRepository userRepository;

    @BeforeEach
    void beforeEach() {
        UserConfiguration userConfiguration = new UserConfiguration();
        userConfiguration.getUsers().add(USER_101);
        userRepository = new UserRepository(userConfiguration);
    }

    @Test
    void givenNoUsersExists_returnEmptyOptional() {
        UserRepository userRepository = new UserRepository(new UserConfiguration());
        Optional<User> user = userRepository.findOne(101L);
        assertThat(user).isEmpty();
    }

    @Test
    void givenUser101Exists_whenFindOneWithId101_returnUser101() {
        Optional<User> user = userRepository.findOne(101L);

        assertThat(user).contains(USER_101);
    }

    @Test
    void givenUser101Exists_whenFindOneWithId102_returnEmptyUser() {
        Optional<User> user = userRepository.findOne(102L);

        assertThat(user).isEmpty();
    }
}
