package com.att.training.demo.user;

import com.att.training.demo.api.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("users")
public class UserController {

    @GetMapping("{id}")
    public User fetch(@PathVariable long id) {
        return new User(101L, "Michael", "Jordan", "https://randomuser.me/api/portraits/men/1.jpg");
    }
}
