package com.att.training.demo.user;


import com.att.training.demo.api.User;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "address-book")
@Data
public class UserConfiguration {

    private final List<User> users = new ArrayList<>();
}
