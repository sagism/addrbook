package com.att.training.demo.user;

import com.att.training.demo.api.User;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;

@Component
class UserRepository {

    private final Map<Long, User> users;

    UserRepository(UserConfiguration userConfiguration) {
        users = userConfiguration.getUsers()
                         .stream()
                         .collect(toMap(User::getId, Function.identity()));
    }

    Optional<User> findOne(long id) {
        User user = users.get(id);
        return Optional.ofNullable(user);
    }
}
