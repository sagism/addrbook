# addressbook

An example of a set of Gherkin [Cypress](http:cypress.io) tests for a simple web app

## Installation
  Under frontend, run: % `npm install`

## Running
  Under frontend:

1. `npm run wsim` (runs the back-end addressbook server simulator)
1. In a different terminal: `npm start` (runs the front-end server)
1. In yet another different server: % `npm test` (brings up cypress)
1. In cypress, click on the `LookupUser.feature`

![output](cypress_screenshot.png "what Cypress looks like after it ran the tests for the LookupUser.feature")


