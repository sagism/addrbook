#!/bin/bash
#
# Set up a simple web hosting project in node
# includes express, handlebars, cypress & its cucumber preprocessor

bailout () {
  echo $1
  exit 1
}

pwd=`pwd`
BASEDIR=$(dirname "$0")
BASEDIR="${pwd}/${BASEDIR}"
frontenddir="frontend"

declare -a templates=(
    "server_template.js"
    "public/css/site_template.css"
    "views/page_template.hbs"
    "cypress/integration/Simple.feature"
    "cypress/integration/Simple/simple.js"
    "cypress/integration/common/common.js"
    )

check_dependency() {
    echo "looking for ${1}"
    if [ ! -f ${1} ]
    then
        bailout "cannot find ${1}"
    fi
}

check_dependencies () {
    for file in "${templates[@]}"
    do
        check_dependency "${BASEDIR}/${file}"
    done
}

echo "verifying template dependencies..."
check_dependencies
echo "all good"


# if we have a repo, clone it, otherwise create a local repo
is_remote_repo=false
read -p "Is there a remote git repository for this project? [Y/n] " YN
[[ $YN == "y" || $YN == "Y" || $YN == "" ]] && is_remote_repo=true

check_project() {
    # make sure project does not already exist so we don't clobber
    # an existing project
    echo "check project: \"${1}\" repo: \"${2}\""
    projname=$1
    reponame=$2

    if [[ ! -z "${reponame}" && "" == "$projname" ]]
    then
        full_repo=`basename ${reponame}`
        projname=${full_repo%%.*}
        echo "project name will be ${projname}"
    fi

    if [ -d $projname ]
    then
        echo "directory ${projname} already exists. aborting"
        exit 1
    fi
}

if $is_remote_repo 
then
    read -p "please enter url for repo: e.g. https://bitbucket.org/sagism/addrbook.git : " repourl
    read -p "enter local project name (or leave empty to use the repo name as project name): " projname
    check_project "${projname}" "${repourl}"
    git clone $repourl $projname || bailout "cannot clone repo"
    cd $projname
else
    read -p "please enter project name: " projname
    check_project "${projname}" 
    mkdir $projname
    cd $projname
    git init || bailout "cannot create local repo"
    git add .
    git commit -m "initial commit"
fi

echo "created project ${projname}"
if [ -d $frontenddir ]
then
    # frontend subdir already exists
    echo ""
else
    mkdir $frontenddir
fi

# install packages
cd $frontenddir
if [ ! -f package.json ]
then
    cp $BASEDIR/package_template.json ./package.json
    sed -i "" "s/PROJNAME/${projname}/g" package.json
fi

echo "Installing node packages..."
npm install

grep node_modules .gitignore > /dev/null 2>&1 || echo "node_modules" >> .gitignore

if [ ! -f server.js ]
then
    echo "Creating web server..."
    cp $BASEDIR/server_template.js ./server.js
fi

copy() {
    src=$1
    dest=$2
    dest_dir=`basename $dest`
    mkdir -p $dest_dir
    cp $src $dest
}

# copy templates
for file in "${templates[@]}"
do
    copy "${BASEDIR}/${file}" "./${file}"
done

echo "DONE!"
echo "cd to ${projname}/${frontenddir} and get going!"
echo "use npm start to start the server"
echo "In another terminal, use npm test to run cypress tests"
echo "use npm run testX to run headless cypress tests"
