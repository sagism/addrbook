const express = require('express');
const app = express();

// Expose static path
const path = require('path');
const publicDirectoryPath = path.join(__dirname, 'public')
console.log('public path:' + publicDirectoryPath)
app.use(express.static(publicDirectoryPath))

// Run like this ADDRBOOK_SVC_URL="http://localhost:8080/demo/users/" bash -c 'npm start'
var serviceURL = process.env.ADDRBOOK_SVC_URL;
if (serviceURL === undefined) {
    serviceURL = "http://localhost:8090/employee/";
}
console.log('Using service at', serviceURL)


app.set('view engine', 'hbs')

app.get('/employee/:id*?', (req, res) => {
    return res.render('employee_lookup.hbs', { 
        employee_id: req.params.id,
        serviceURL: serviceURL
        });
});

const port = 5000;
app.listen(port, () => {
    console.log(`address server running on port ${port}`);
});