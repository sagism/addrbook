import { Given, Then } from "cypress-cucumber-preprocessor/steps";
var scenario_start;

Given('I open the homepage', () => {
    cy.visit("/")
})

Given(`I navigate to the {string} page`, (url) => {
    cy.visit(url)
})

Then(`I see {string} in the title`, (title) => {
  cy.title().should('include', title)
})

Then(`the page contains {string}`, (text) => {
  const re = new RegExp(text,"i");
  cy.contains(re)
})

Then(`the page contains exactly {string}`, (text) => {
  cy.contains(text)
})

Given('I use a {string} device', (device) => {
  cy.viewport(device);
});

Then('I type {string}', (text) => {
  cy.focused().type(text)
})