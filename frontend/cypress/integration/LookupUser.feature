Feature: Look up a user by id

  As an People Admin I want to look up a user by id so I can verify a specific user's details

  Scenario: Look up an existing employee
    Given I navigate to the employee lookup page
    When I look up an existing employee id
    Then Employee name field contains a name

  Scenario: Look up a specific existing employee
    Given I navigate to the employee lookup page
    When I look up employee id "101"
    Then Employee name field contains "Holy Mitchell"

  Scenario: Look up an empty employee
    Given I navigate to the employee lookup page
    When I look up an empty employee id
    Then the page contains "Please specify a valid ID"

  Scenario: Look up a garbage employee id
    Given I navigate to the employee lookup page
    When I look up employee id "和平"
    Then the page contains "Please specify a valid ID"

  Scenario: Look up an non-existent employee
    Given I navigate to the employee lookup page
    When I look up a non-existing employee id
    Then Employee name is empty
    Then the page contains "Not found"

  Scenario: Lookup erases previous result
    Given I navigate to the employee lookup page
    When I look up an existing employee id
    And I look up a non-existing employee id
    Then Employee name is empty

  Scenario: Look up an existing employee using direct link
    Given I navigate to the direct url for an existing employee
    Then Employee name field contains a name

  Scenario: Service is down
    Given I navigate to the employee lookup page
    And the service is unreachable
    When I look up an existing employee id
    Then the page contains "Please try again later"

  Scenario: Service is too slow
    Given I navigate to the employee lookup page
    And the service is slow
    When I look up an existing employee id
    Then the page contains "Please try again later"

  Scenario: Search for another employee
    Given I navigate to the employee lookup page
    When I look up an existing employee id
    Then I can perform a search

  Scenario: Responsivity
    Given I use a 'iphone-6' device
    And I navigate to the employee lookup page
    Then I see the search button
  
  Scenario: Keyboard navigability
    Given I navigate to the employee lookup page
    And I monitor lookup operations
    Then I type "5555"
    When I type "{enter}"
    And a lookup operation is performed 

  Scenario Outline: Allow separators in employee id
    Given I navigate to the employee lookup page
    When I look up employee id <id>
    Then I find text <cleaned_id>

  Examples:
    |         id  |     cleaned_id   |
    |      "1234" |     "User 1234"  |
    |     "12-34" |     "User 1234"  |
    |   "1.2.3.4" |     "User 1234"  |
    |   "6 7 888" |     "User 67888" |

  Scenario: Search should be fast 
    Given I navigate to the employee lookup page
    When I look up an existing employee id
    Then searching should take no more than 300 ms
