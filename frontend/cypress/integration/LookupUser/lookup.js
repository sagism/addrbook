import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";

const employee_url = "employee/";
const non_existing_employee_id = "3445";
const serviceUrl = "http://localhost:8090/employee/"

const lookup_employee = (id) => {
    cy.get('[data-cy=employee_id]').clear().type(id)
    cy.get('[data-cy=do_lookup]').click()
}

Given('I navigate to the employee lookup page', () => {
    cy.visit(employee_url)
})

When('I look up an existing employee id', () => {
    cy.fixture('employee').then((existing_employee) => {
        lookup_employee(existing_employee.id);
    });
});

Then('Employee name field contains a name', () => {
    cy.get('[data-cy=employee_name]').contains(/\S\s\S/)
});

When('I look up employee id {string}', (id) => {
    lookup_employee(id)
});

Then('Employee name field contains {string}', (text) => {
    cy.get('[data-cy=employee_name]').contains(text)
});

When('I look up a non-existing employee id', () => {
    lookup_employee(non_existing_employee_id)
});

Then('I get the details page for {string}', (name) => {
    cy.contains(name);
});

Then('I find text {string}', (text) => {
    var regex = new RegExp(text);
    cy.contains(regex);
});

Then('I get the details page for id {string}', (id) => {
    cy.get('[data-cy=effective_employee_id]').contains(id)
    cy.get('[data-cy=effective_employee_id]').should('have.text', id)
});

Then('I see the search button', () => {
    cy.get('[data-cy=do_lookup]').should('be.visible')
});

When('the service is slow', () => {
    cy.server()
    cy.route({ // create an unacceptable delay
        method: 'GET',
        url: serviceUrl + '**',
        response: [],
        delay: 3000
    })
});

When('the service is unreachable', () => {
    cy.server()
    var url = serviceUrl + '*';
    cy.route({ // fail
        method: 'GET',
        url: url,
        status: 500,
        response: []
    })
});

Then('I can perform a search', () => {
    cy.get('[data-cy=employee_id]')
    cy.get('[data-cy=do_lookup]')
});

When('I look up an empty employee id', () => {
    cy.get('[data-cy=employee_id]')
    cy.get('[data-cy=do_lookup]').click();
})

Given('I monitor lookup operations', () => {
    cy.server();
    cy.route(serviceUrl + '*', 'fixture:employee').as('lookup')
});

Then('a lookup operation is performed', () => {
    cy.wait('@lookup')
    cy.get('[data-cy=employee_name]')
});

Then('searching should take no more than {int} ms', (timeout) => {
    cy.get('[data-cy=employee_name]', {timeout: timeout})
});
    
Then('Employee name is empty', () => {
    cy.get('[data-cy=employee_name]').should('not.exist')
});

When('I navigate to the direct url for an existing employee', () => {
    cy.fixture('employee').then((existing_employee) => {
        cy.visit(employee_url + existing_employee.id);
    });
});

