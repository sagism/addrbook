var app;

function get_employee(id) {
    app.employee_id = null;
    if (id === "") {
        app.message = "Please specify a valid ID";
        return;
    }
    var url = serviceURL + id;
        console.log("requesting ", url);
        app.request_in_progress = true;
        $.getJSON( {
            url : url,
            timeout : 1000
        }, function(data) {
            console.log( "success getting data for employee " + id );
            app.employee_id = data.id;
            app.first = data.firstName;
            app.last = data.lastName;
            app.pic = data.photo;
            app.message = null;
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log("error accessing service: " + textStatus);
            if( textStatus == "timeout" ) {
                // do stuff in case of timeout
                app.message = "Error: Timeout accessing the service. Please try again later.";
            } else if ( errorThrown == "Not Found" ) {
                app.message = "Error: User " + id + " not found";
            } else {
                app.message = "Error: cannot access service. Please try again later.";   
            }
        })
        .always(function(){
            window.history.pushState({page: "another"}, "another page", id);
            app.request_in_progress = false;
        });
}

$(document).ready(function(){
    $('#do-lookup').click(function(){
        var employee_id = $('#employee-id-input').val();
        var clean_employee_id = employee_id.replace(/\D/g,'');
        get_employee(clean_employee_id); 
    });

    app = new Vue({
        el: '#app',
        data: {
          message: null,
          employee_id: null,
          request_in_progress: false,
          blah: false
        }
      });
});