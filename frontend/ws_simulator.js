const express = require('express');

/*
    Mock Address Book Service
    Test from the command line using:
    % curl -i http://localhost:8090/employee/1234 ; echo
    Or from a browser with the same link
    Uses employee from cypress fixture
*/
const app = express();

const fs = require('fs');
let rawdata = fs.readFileSync('cypress/fixtures/employee.json');  
let sample_employee = JSON.parse(rawdata);  

app.get('/employee/:id', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    console.log(`Got a request to display ${req.params.id}`);
    var user_id = req.params.id;

    if (user_id==sample_employee.id) {
        var responseData = sample_employee;
        return res.status(200).send(responseData);
    } else {
        console.log('employee not found')
        var responseData = {
            code: 5001,
            message: `ID ${user_id} not found`
        }
        return res.status(404).send(responseData);
        
    }
});


const port = 8090;
app.listen(port, () => {
    console.log(`AddressBook Service running on port ${port}`);
});

